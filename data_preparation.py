import glob, os
import librosa.display
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import specgram


sound_path = '/home/dim/datasets/bird-sounds/train/nips4b_birds_trainfile292.wav'
basename = os.path.basename(sound_path)
data_name = os.path.splitext(basename)[0]

data, sampling_rate = librosa.load(sound_path)

fig = plt.gcf()
fig_size = fig.get_size_inches()
w,h = fig_size[0], fig_size[1]
fig.patch.set_alpha(0)



a = fig.gca()
a.set_frame_on(False)
a.set_xticks([])
a.set_yticks([])
plt.axis('off')
#plt.xlim(0, h)
#plt.ylim(w, 0)

librosa.display.waveplot(data, sr= sampling_rate)
fig.savefig('wave.png', bbox_inches='tight', pad_inches=0)

#specgram(np.array(data), Fs=22500)
#fig.savefig('spectogram.png', bbox_inches='tight', pad_inches=0)
dir_project = '/home/dim/Documents/5-IBD/DeepLearning/project/Bird-sounds/'

def get_sounds_features(data, data_name):
    # get chromagram_stf
    chroma_stft = librosa.feature.chroma_stft(y=data, sr=sampling_rate)
    librosa.display.specshow(chroma_stft)
    fig.savefig(dir_project + 'chroma_stft/chroma_stft-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

    #get Constant-Q chromagram
    chroma_cqt = librosa.feature.chroma_cqt(y=data, sr=sampling_rate)
    librosa.display.specshow(chroma_cqt)
    fig.savefig(dir_project + 'chroma_cqt/chroma_cqt-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

    #get Chroma energy Normalized
    chroma_cens = librosa.feature.chroma_cens(y=data, sr=sampling_rate)
    librosa.display.specshow(chroma_cens)
    fig.savefig(dir_project + 'chroma_cens/chroma_cens-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

    #get mel-scaled spectrogram
    melspectrogram = librosa.feature.melspectrogram(y=data, sr=sampling_rate)
    librosa.display.specshow(melspectrogram)
    fig.savefig(dir_project + 'melspectrogram/melspectrogram-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

    #get Mel-frequency cepstral coefficient
    mfcc = librosa.feature.mfcc(y=data, sr=sampling_rate)
    librosa.display.specshow(mfcc)
    fig.savefig(dir_project + 'mfcc/mfcc-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

    #get Root-mean-square
    rmse = librosa.feature.rmse(y=data)
    librosa.display.specshow(rmse)
    fig.savefig(dir_project + 'rmse/rmse-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

    #get spectral bandwitdh
    spectral_bandwidth = librosa.feature.spectral_bandwidth(y=data, sr=sampling_rate)
    librosa.display.specshow(spectral_bandwidth)
    fig.savefig(dir_project + 'spectral_bandwidth/spectral_bandwidth-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

    #get spectral centroid
    spectral_centroid = librosa.feature.spectral_centroid(y=data, sr=sampling_rate)
    librosa.display.specshow(spectral_centroid)
    fig.savefig(dir_project + 'spectral_centroid/spectral_centroid-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

    #get spectral contrast
    spectral_contrast = librosa.feature.spectral_contrast(y=data, sr=sampling_rate)
    librosa.display.specshow(spectral_contrast)
    fig.savefig(dir_project + 'spectral_contrast/spectral_contrast-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

    #get roll-off frequency
    spectral_rolloff = librosa.feature.spectral_rolloff(y=data, sr=sampling_rate)
    librosa.display.specshow(spectral_rolloff)
    fig.savefig(dir_project + 'spectral_rolloff/spectral_rolloff-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

    #get tonal centroid
    tonnetz = librosa.feature.tonnetz(y=data, sr=sampling_rate)
    librosa.display.specshow(tonnetz)
    fig.savefig(dir_project + 'tonnetz/tonnetz-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

    #get zero-crossing rate
    zero_crossing_rate = librosa.feature.zero_crossing_rate(y=data, frame_length=len(data))
    librosa.display.specshow(zero_crossing_rate)
    fig.savefig(dir_project + 'zero_crossing_rate/zero_crossing_rate-' + data_name + '.png', bbox_inches='tight', pad_inches=0)

get_sounds_features(data, data_name)


